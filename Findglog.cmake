#.rst
# Findglog
# --------
#
# Find Google log library.
# Module adds glog::glog imported target.
# Additionally GLOG_INCLUDE_DIRS and GLOG_LINK_LIBRARIES are set.
#
# License: 3-Clause BSD License
#
#  Copyright 2018 Piotr Lukasik
#
#  Redistribution and use in source and binary forms, with or without
#  modification, are permitted provided that the following conditions are met:
#  
#  1. Redistributions of source code must retain the above copyright notice,
#     this list of conditions and the following disclaimer.
#  
#  2. Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions and the following disclaimer in the documentation
#     and/or other materials provided with the distribution.
#  
#  3. Neither the name of the copyright holder nor the names of its contributors
#     may be used to endorse or promote products derived from this software
#     without specific prior written permission.
#  
#  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
#  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
#  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
#  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
#  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
#  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
#  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
#  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
#  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
#  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
#  POSSIBILITY OF SUCH DAMAGE.

include(FindPackageHandleStandardArgs)

find_package(PkgConfig)
if(PKG_CONFIG_FOUND)
  pkg_check_modules(GLOG_pc libglog)
endif()

find_path(GLOG_logging_INCLUDE_DIR glog/logging.h HINTS "${GLOG_pc_INCLUDE_DIRS}")
find_library(GLOG_glog_LIBRARY glog HINTS "${GLOG_pc_LIBRARY_DIRS}")

find_package_handle_standard_args(glog
  REQUIRED_VARS GLOG_logging_INCLUDE_DIR GLOG_glog_LIBRARY
  VERSION_VAR GLOG_pc_VERSION)

mark_as_advanced(GLOG_logging_INCLUDE_DIR GLOG_glog_LIBRARY)

set(GLOG_INCLUDE_DIRS "${GLOG_logging_INCLUDE_DIR}")
set(GLOG_LINK_LIBRARIES "${GLOG_glog_LIBRARY}")

add_library(glog::glog UNKNOWN IMPORTED)
set_target_properties(glog::glog PROPERTIES
  IMPORTED_LOCATION "${GLOG_LINK_LIBRARIES}"
  INTERFACE_INCLUDE_DIRECTORIES "${GLOG_INCLUDE_DIRS}"
)

