#.rst
# FindSASL2
# ---------
#
# Find Cyrus SASL 2 library, headers and plugins directory.
# Module adds Cyrus::SASL2 imported target.
# Additionally SASL2_INCLUDE_DIRS and SASL2_LINK_LIBRARIES are set.
#
# Module also searches for Cyrus SASL's plugin directory. If found, it is stored
# in SASL2_PLUGIN_DIR variable.
#
# License: 3-Clause BSD License
#
#  Copyright 2018 Piotr Lukasik
#
#  Redistribution and use in source and binary forms, with or without
#  modification, are permitted provided that the following conditions are met:
#  
#  1. Redistributions of source code must retain the above copyright notice,
#     this list of conditions and the following disclaimer.
#  
#  2. Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions and the following disclaimer in the documentation
#     and/or other materials provided with the distribution.
#  
#  3. Neither the name of the copyright holder nor the names of its contributors
#     may be used to endorse or promote products derived from this software
#     without specific prior written permission.
#  
#  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
#  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
#  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
#  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
#  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
#  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
#  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
#  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
#  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
#  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
#  POSSIBILITY OF SUCH DAMAGE.

include(FindPackageHandleStandardArgs)

find_package(PkgConfig)
if(PKG_CONFIG_FOUND)
  pkg_check_modules(SASL2_pc libsasl2)
endif()

find_path(SASL2_sasl_INCLUDE_DIR sasl/sasl.h HINTS "${SASL2_pc_INCLUDE_DIRS}")
find_library(SASL2_sasl2_LIBRARY sasl2 HINTS "${SASL2_pc_LIBRARY_DIRS}")

find_package_handle_standard_args(SASL2
  REQUIRED_VARS SASL2_sasl_INCLUDE_DIR SASL2_sasl2_LIBRARY
  VERSION_VAR SASL2_pc_VERSION)

mark_as_advanced(SASL2_sasl_INCLUDE_DIR SASL2_sasl2_LIBRARY)

set(SASL2_INCLUDE_DIRS "${SASL2_sasl_INCLUDE_DIR}")
set(SASL2_LINK_LIBRARIES "${SASL2_sasl2_LIBRARY}")

add_library(Cyrus::SASL2 UNKNOWN IMPORTED)
set_target_properties(Cyrus::SASL2 PROPERTIES
  IMPORTED_LOCATION "${SASL2_LINK_LIBRARIES}"
  INTERFACE_INCLUDE_DIRECTORIES "${SASL2_INCLUDE_DIRS}"
)
  
# Cyrus SASL plugin directory search:
if(SASL2_FOUND)
  get_filename_component(
    SASL2_sasl_LIBRARY_DIR "${SASL2_sasl2_LIBRARY}" DIRECTORY) 
  
  #'ntlm' 'otp' and 'sql' plugins skipped to avoid false positives. 
  find_library(SASL2_any_plugin_LIBRARY
    NAMES sasldb crammd5 digestmd5 login plain scram anonymous 
    PATH_SUFFIXES sasl2
    HINTS ${SASL2_sasl_LIBRARY_DIR})
  mark_as_advanced(SASL2_any_plugin_LIBRARY)
  
  if(SASL2_any_plugin_LIBRARY)
    get_filename_component(
      SASL2_any_plugin_LIBRARY_DIR "${SASL2_any_plugin_LIBRARY}" DIRECTORY)
    set(SASL2_PLUGIN_DIR ${SASL2_any_plugin_LIBRARY_DIR} CACHE PATH
      "Path to Cyrus SASL plugins directory")
  endif()
endif()

